<?php

namespace Google2FA;

class InvalidLengthException extends \Exception {

    protected $message = 'Secret key is too short. Must be at least 16 base 32 characters';

    public function __construct($message) {
        parent::__construct($message);
    }

}
