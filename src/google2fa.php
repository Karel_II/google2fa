<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        'Google2FA\\InvalidLengthException' => 'exceptions.php',
        'Google2FA\\Google2FA' => 'Google2FA.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/Google2FA/' . $classMap[$className];
    }
});

